package net.rohbot.gps;


import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("HandlerLeak")
public class MainActivity extends Activity {
	
	private Button button;
	private TextView textBox;
	
	private GpsTracker gps;
	
	private final Handler handler = new Handler(){
		
		@Override
		public void handleMessage(Message message){
			if(message.getData().containsKey(GpsTracker.MESSAGE_STRING)){
				String msg = message.getData().getString(GpsTracker.MESSAGE_STRING);
				updateTextBox(msg);
			}else if(message.getData().containsKey(GpsTracker.GPS_STATUS_STRING)){
				boolean gps_status = message.getData().getBoolean(GpsTracker.GPS_STATUS_STRING);
				if(gps_status){
					Toast.makeText(getApplicationContext(), "GPS Ready!", Toast.LENGTH_SHORT).show();
					button.setEnabled(true);
				}
				else{
					Toast.makeText(getApplicationContext(), "GPS Unavailable!", Toast.LENGTH_SHORT).show();
					
				}
			}
			else{
				Bundle data = message.getData();
				double lat = data.getDouble(GpsTracker.LATITUDE_STRING);
				double lng = data.getDouble(GpsTracker.LONGITUDE_STRING);
	            updateTextBox("lat: " + lat + " lng: " + lng );
			}
		} 
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		button = (Button) findViewById(R.id.startBtn);
		textBox = (TextView) findViewById(R.id.textView);
		gps = new GpsTracker(this, handler);
		button.setEnabled(false);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void buttonClick(View view){
		if(button.getText().equals("Start")){
			startGps();
			button.setText("Stop");
		}else{
			stopGps();
			button.setText("Start");
		}
		
	}
	
	private void updateTextBox(String text){
		textBox.append(text + '\n');
	}
	
	private void startGps(){
		updateTextBox("Starting GPS....");
		gps.startPolling(); 
	}
	
	private void stopGps(){

		updateTextBox("Stopping GPS....");
		gps.stopPolling();
	}
	
}
