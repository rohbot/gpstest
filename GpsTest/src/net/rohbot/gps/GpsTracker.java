package net.rohbot.gps;


import android.content.Context;
import android.location.GpsStatus;
import android.location.GpsStatus.Listener;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

public class GpsTracker implements LocationListener, Listener {

	private final Context mContext;
	 
    // flag for GPS status
    boolean isGPSEnabled = false;
 
    // flag to show if location data is available
    boolean canGetLocation = false;
 
    Location location; // location
    double latitude; // latitude
    double longitude; // longitude
 
    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 1; // 2 meters
 
    // The minimum time between updates in milliseconds
    private long pollingDelay = 1000*2; // 2 seconds

	public static final String LATITUDE_STRING = "latitude"; 
    public static final String LONGITUDE_STRING = "longitude"; 
    public static final String ELEVATION_STRING = "elevation";
    public static final String SPEED_STRING = "speed";
    public static final String GPS_STATUS_STRING = "GPS_ON";
    public static final String MESSAGE_STRING = "message";
    
    
    
    // Declaring a Location Manager
    protected LocationManager locationManager;
    
    private GpsStatus gpsStatus;
    
    private final Handler uiThreadHandler;
	
    private boolean polling = false;
    
    public GpsTracker(Context context, Handler handler) {
        this.mContext = context;
        this.uiThreadHandler = handler;
        
        locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        locationManager.addGpsStatusListener(this);    
        if (locationManager != null) {
            // getting GPS status
            isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            //start polling
    		locationManager.requestLocationUpdates(
	                LocationManager.GPS_PROVIDER,
	                pollingDelay,
	                0, this);
        }
    }
    
    
  /**
   * method called when new Location is obtained from GPS
   * sends a message on the Handler back to UI with various attributes
   * of new Location
   */
	@Override
	public void onLocationChanged(Location newLocation) {
		this.location = newLocation;
		//sendMessageToUI("lat: " + newLocation.getLatitude() + " lng: " + newLocation.getLongitude());
		if(!canGetLocation){
			canGetLocation = true;
			//Send message to say GPS is ready
			sendGpsStatusToUI(true);
		}
		
		//only send updated location to UI if polling
		if(polling){
			Message msg = uiThreadHandler.obtainMessage();
			Bundle b = new Bundle();
			b.putDouble(LATITUDE_STRING, newLocation.getLatitude());
			b.putDouble(LONGITUDE_STRING, newLocation.getLongitude());
			
			if(newLocation.hasAltitude())
				b.putDouble(ELEVATION_STRING, newLocation.getAltitude());
			if(newLocation.hasSpeed())
				b.putDouble(SPEED_STRING, newLocation.getSpeed());
			
			msg.setData(b);
			uiThreadHandler.sendMessage(msg);
		}
	}

    
    /**
     * Start the GPS listener
     * need to call this function to begin polling
     */
    public void startPolling(){
    	polling = true;
    	if(isGPSEnabled){
    		
    		locationManager.requestLocationUpdates(
	                LocationManager.GPS_PROVIDER,
	                pollingDelay,
	                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
	        
	        Log.d("GPS Enabled", "GPS Enabled");
	        //sendMessageToUI("GPS started!");
	        	
    	}
    
    }
    
    /**
     * Stop using GPS listener
     * Calling this function will stop using GPS in your app
     * */
    public void stopPolling(){
    	polling = false;
        if(isGPSEnabled){
            locationManager.removeUpdates(GpsTracker.this);
            canGetLocation = false;
            sendMessageToUI("GPS Stopped!");
        }      
    }
    

	public long getPollingDelay() {
		return pollingDelay;
	}

	/**
	 * Sets the minimum time between GPS updates 
	 * 
	 * @param pollingDelay in milliseconds
	 */
	public void setPollingDelay(long pollingDelay) {
		this.pollingDelay = pollingDelay;
		
		// update time in location manager if polling is on
    	if(polling){
    		// remove previous callback
            locationManager.removeUpdates(GpsTracker.this);
            
            //update to correct polling delay 
    		locationManager.requestLocationUpdates(
	                LocationManager.GPS_PROVIDER,
	                pollingDelay,
	                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
	        
	        Log.d("GPS Polling Delay", "new delay: " + pollingDelay);
	        //sendMessageToUI("GPS started!");
	        	
    	}

		
	}

    
    private void sendMessageToUI(String text){
    	Message msg = uiThreadHandler.obtainMessage();
		Bundle b = new Bundle();
		b.putString(MESSAGE_STRING, text);
		msg.setData(b);
		uiThreadHandler.sendMessage(msg);
    }
    
    private void sendGpsStatusToUI(boolean status){
    	Message msg = uiThreadHandler.obtainMessage();
		Bundle b = new Bundle();
		b.putBoolean(GPS_STATUS_STRING, status);
		msg.setData(b);
		uiThreadHandler.sendMessage(msg);
    }
    
    /**
     * Function to get latitude
     * */
    public double getLatitude(){
        if(location != null){
            latitude = location.getLatitude();
        }
         
        // return latitude
        return latitude;
    }
     
    /**
     * Function to get longitude
     * */
    public double getLongitude(){
        if(location != null){
            longitude = location.getLongitude();
        }
         
        // return longitude
        return longitude;
    }
     
    /**
     * Function to check GPS/wifi enabled
     * @return boolean
     * */
    public boolean canGetLocation() {
        return this.canGetLocation;
    }
     

	@Override
	public void onProviderDisabled(String provider) {
		sendGpsStatusToUI(false);
	}

	@Override
	public void onProviderEnabled(String provider) {
	}

	@Override
	public void onStatusChanged(String provide, int status, Bundle extras) {
		
	}


	@Override
	public void onGpsStatusChanged(int event) {
		 gpsStatus = locationManager.getGpsStatus(gpsStatus);
		    switch (event) {
		        case GpsStatus.GPS_EVENT_STARTED:
		        	sendMessageToUI("GPS Available!");
		            break;
		        case GpsStatus.GPS_EVENT_STOPPED:
		            sendGpsStatusToUI(false);
		            break;
		        case GpsStatus.GPS_EVENT_FIRST_FIX:
		        	sendGpsStatusToUI(true);
		            break;
		        case GpsStatus.GPS_EVENT_SATELLITE_STATUS:
		            break;
		    }
		
	}



}
